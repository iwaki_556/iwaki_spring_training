package sample007_bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main001 {
	public static void main(String args[]) {
		
		//XMLでセッターインジェクションを行う
		
		ApplicationContext context = new ClassPathXmlApplicationContext("sample007_bean/ApplicationContext.xml");
		Calc calc = (CalcImpl)context.getBean("calc");
		
		calc.result(5, 10);
		
		
		calc = (CalcImpl2)context.getBean("calc2");
		calc.result(5, 10);
		
	}
}
