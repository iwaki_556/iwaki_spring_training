package sample002_bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main001 {
	public static void main(String args[]) {
		
		
		//アノテーションでBean定義した場合
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		Calc calc = context.getBean(Calc.class);
		
		calc.result(5, 10);
		
	}
}
