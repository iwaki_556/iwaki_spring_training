package sample002_bean;

import org.springframework.stereotype.Component;

@Component
public class SumImpl implements Sum {
	@Override
	public int add(int a, int b) {
		return a + b;
	}
}
