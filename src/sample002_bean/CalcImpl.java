package sample002_bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CalcImpl implements Calc{
	private Sum sum;
	private Subtract subtract;
	
	@Autowired
	public CalcImpl(Sum sum, Subtract subtract) {
		this.sum = sum;
		this.subtract = subtract;
	}
	
	@Override
	public int result(int a, int b) {
		System.out.println(this.sum.add(a, b));
		return this.sum.add(a, b);
		
	}
}
