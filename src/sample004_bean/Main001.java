package sample004_bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main001 {
	public static void main(String args[]) {
		
		//アノテーションによるフィールドインジェクションを行う
		
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		Calc calc = context.getBean(Calc.class);
		
		calc.result(5, 10);
		
	}
}
