package sample009_aop;

import org.springframework.stereotype.Component;

@Component
public class IntegerIF2 implements IF<Integer> {
	
	public IntegerIF2() {
		System.out.println("instanced:IntegerIF2");
	}
	
	public void result() {
		System.out.println("Called IntegerIF2");
	}
}
