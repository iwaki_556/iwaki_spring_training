package sample009_aop;

import org.springframework.stereotype.Component;

@Component
public class IntegerIF1 implements IF<Integer> {
	
	public IntegerIF1() {
		System.out.println("instanced:IntegerIF1");
	}
	
	public void result() {
		System.out.println("called IntegerIF1");
	}
}
