package sample009_aop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main001 {

	public static void main(String[] args) {
		
		/*
		 * AOPの例（@After）、Autowireする時は、インタフェースに対して行う。実装されたクラスに対して行うと、正常にインジェクションされない。（エラーが出る）
		 * 
		 * 参照URL：http://kamatama41.hatenablog.com/entry/20140327/1395928048
		 * SpringAOPのプロキシ化の仕組みにはJDK dynamic proxyとCGLIBという二つの仕組みがあるんですが
		 * (デフォルトはJDK dynamic proxy)JDK dynamic proxyでProxy化されたbeanのインスタンスを直接実装クラス指定でAutowired出来ない仕様らしいです。
		 * インターフェースを介さないメソッドをProxy化する場合のみ
		 * CGLIB proxyingを使いましょうというのがSpringAOPのポリシーらしいです。
		 * それを鑑みて、UserDaoをProxy化したbeanはUserDaoImplではないという判断になっているのではないでしょうか？
		 */
		
		
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		Run run = context.getBean(Run.class);
		run.start();
		((ConfigurableApplicationContext)context).close();
	}
}
