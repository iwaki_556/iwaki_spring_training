package sample009_aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("sample009_aop")
@EnableAspectJAutoProxy
public class AppConfig {
}
