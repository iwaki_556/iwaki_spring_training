package sample005_bean;

public class CalcImpl implements Calc{
	private Sum sum;
	private Subtract subtract;
	
	
	public Sum getSum() {
		return sum;
	}


	public void setSum(Sum sum) {
		this.sum = sum;
	}


	public Subtract getSubtract() {
		return subtract;
	}


	public void setSubtract(Subtract subtract) {
		this.subtract = subtract;
	}


	@Override
	public int result(int a, int b) {
		System.out.println(this.sum.add(a, b));
		return this.sum.add(a, b);
		
	}
}
