package sample005_bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
	@Bean
	Sum sum() {
		return new SumImpl();
	}
	
	@Bean
	Subtract subtract() {
		return new SubtractImpl();
	}
	
	@Bean
	Calc calc(Sum sum, Subtract subtract) {
		CalcImpl calc = new CalcImpl();
		calc.setSubtract(subtract);
		calc.setSum(sum);
		return calc;
	}
}
