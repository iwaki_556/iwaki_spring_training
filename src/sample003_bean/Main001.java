package sample003_bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main001 {
	public static void main(String args[]) {
		
		//XMLでBean定義した場合
		
		ApplicationContext context = new ClassPathXmlApplicationContext("sample003_bean/ApplicationContext.xml");
		Calc calc = context.getBean(Calc.class);
		
		calc.result(5, 10);
		
	}
}
