package sample010_aop;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("sample010_aop")
@EnableAspectJAutoProxy
public class AppConfig {
}
