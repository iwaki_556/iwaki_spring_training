package sample010_aop;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Component
public class RunImpl implements Run{

	@Autowired
	 IF<String> a;

	@Autowired
	List<IF> listIf;

	@Autowired
	Map<String, IF> mapIf;
	
	@Autowired
	@Qualifier("stringIF")
	IF as;


	public void start() {
		System.out.println("IFのbeanの数 = " + listIf.size());

		mapIf.get("integerIF1").result();
		a.result();

	}
}
