package sample010_aop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main001 {

	public static void main(String[] args) {
		
		//AOPの例（@AfterReturning）
		
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		Run run = context.getBean(Run.class);
		run.start();
		((ConfigurableApplicationContext)context).close();
	}
}

