package sample010_aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MethodStartLoggingAspect {
	@AfterReturning(value = "execution(* sample010_aop..*.*(..))", returning = "user")
	public void endLog(JoinPoint jp, Object user) {
		System.out.println("メソッド正常終了: " + jp.getSignature() + "戻り値: " + user);
	}
}
