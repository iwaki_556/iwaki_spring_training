package sample010_aop;

import org.springframework.stereotype.Component;

@Component
public class StringIF implements IF<String> {
	
	public StringIF() {
		System.out.println("instanced:StringIF");
	}
	
	public void result() {
		System.out.println("called StringIF");
	}
}
