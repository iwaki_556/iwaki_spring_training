package sample008_autowire;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main001 {

	public static void main(String[] args) {
		
		//色々なAutowireを試す
		
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		context.getBean(Run.class).start();
		((ConfigurableApplicationContext)context).close();
	}
}
